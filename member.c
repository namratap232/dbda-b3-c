#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void member_area(user_t *u) 
{
    printf("\n***Member Menu***");
	int choice;
	char name[80];
	do {
		printf("\n\n0. Sign Out\n1. Find Book\n2. Edit Profile\n3. Change Password\n4. Book Availability\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) 
        {
			case 1: // Find Book
				printf("Enter book name: ");
				scanf("%s", name);
				book_find_by_name(name);
				break;
			case 2: // Edit Profile
				profile_edit_by_id(u->id);
				break;
			case 3: // Change Password
				change_password(u->id);
				break;
			case 4: // Book Availability
				bookcopy_checkavail();
				break;
		}
	}while (choice != 0);	
}

void bookcopy_checkavail() 
{
	int book_id;
	FILE *fp;
	bookcopy_t bc;
	int count = 0;
	// input book id
	printf("\nEnter book id: ");		scanf("%d", &book_id);
	// open book copy file
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL) 
	{
		perror("\nFailed to open book coopy file.");
		return;
	}

	// read bookcopy records one by one
	while(fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) 
	{
		// if book id is matching and status is available, count the copies
		if(bc.bookid == book_id && strcmp(bc.status, STATUS_AVAIL) == 0) 
		{
		//	bookcopy_display(&bc);
			count++;
		}
	}
	// close book copy file
	fclose(fp);
	// print the message. 
	printf("\nNumber of copies availables: %d", count);
}
