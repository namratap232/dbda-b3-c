#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void sign_in()
{
    char email[30], password[10];
    user_t u;
    int invalid_user = 0;
    // input email and password from user
    printf("\nEnter email : ");     scanf("%s", email);
    printf("\nEnter password : ");  scanf("%s", password);
    
    if(user_find_by_email(&u,email) == 1)
    {
        if (strcmp(password, u.password) == 0)
        {
            printf("\nHello %s, you Signed In successfully.",u.name.fname);
            if (strcmp(email, EMAIL_OWNER) == 0)
                strcpy(u.role, ROLE_OWNER);

            if (strcmp(u.role, ROLE_OWNER) == 0)
                owner_area(&u);
            else if (strcmp(u.role,ROLE_LIBRARIAN) == 0)
                librarian_area(&u);
            else if (strcmp(u.role,ROLE_MEMBER) == 0)
                member_area(&u);
            else 
                invalid_user = 1;            
        }
        else 
            invalid_user = 1;
    }
    else
        invalid_user = 1;
    
    if(invalid_user == 1)
        printf("\nInvalid user.");
}

void sign_up()
{
    // input user details from the user
    user_t u;
    user_accept(&u);
    user_display(&u);
    // write user details into user's file
    user_add(&u);

}

int main()
{
    printf("\n***LIBRARY MANAGEMENT***");
    int choice;
    do {
		printf("\n\n0. Exit\n1. Sign In\n2. Sign Up\nEnter choice: ");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1: // Sign In
			sign_in();
			break;
		case 2:	// Sign Up
			sign_up();	
			break;
		}
	}while(choice != 0);
    
    return 0;
}
