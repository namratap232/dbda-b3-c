#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"
#include "date.h"


// user functions
void user_accept(user_t *u) 
{
	//printf("id: ");               //scanf("%d", &u->id);
	u->id = get_next_user_id();     //auto generated id
	printf("\nEnter name (Fn Ln): ");  scanf("%s %s", u->name.fname,u->name.lname);
    printf("\nEnter email : ");        scanf("%s", u->email);
	printf("\nEnter phone: ");         scanf("%s", u->phone);
	printf("\nEnter password: ");      scanf("%s", u->password);
	strcpy(u->role, ROLE_MEMBER);    //by default role is member
}

void user_display(user_t *u) 
{
	printf("\nID : %d",u->id);
    printf("\tName : %s %s",u->name.fname,u->name.lname); 
    printf("\tEmail : %s",u->email);
    printf("\tPhone : %s",u->phone);
    printf("\tRole : %s",u->role);
}

void user_add(user_t *u) 
{
	// open the file for appending the data in binary fromat --> "ab"
    FILE *fp;
    fp = fopen(USER_DB,"ab");
    if (fp == NULL)
    {
        perror("\nFailed to open users file.");
        return;
    }    

	// write user data into the file
    fwrite(u, sizeof(user_t), 1, fp);
    printf("\nUser added into file.");
	
	// close the file
    fclose(fp);
}

int get_next_user_id()
{
    FILE *fp;
    int max = 0;
    int size = sizeof(user_t);
    user_t u;
    // open the file
    fp = fopen(USER_DB,"rb");
    if (fp == NULL)
        return max + 1;    
	
	// change file pos to the last record
    fseek(fp, -size, SEEK_END);
	// read the record from the file
    if(fread(&u, size, 1, fp) > 0)
        // if read is successful, get max (its) id
        max = u.id;	

	// close the file
    fclose(fp);
	// return max + 1
    return max +1;
}

int user_find_by_email(user_t *u, char email[])
{
    FILE *fp;
    int found = 0;
    // open the file for reading the data
    fp = fopen(USER_DB,"rb");
    if (fp == NULL)
    {
        perror("\nFailed to open user file.");
        return found;
    }    
    // read all users one by one
    while(fread(u,sizeof(user_t), 1, fp) > 0)
    {
        // if user email is matching, found 1
        if (strcmp(u->email,email) == 0)
        {
            found = 1;
            break;
        }
    }
    // close file
    fclose(fp);
    //return found
    return found;
}

// book functions
void book_accept(book_t *b) 
{
	// printf("id: ");              	// scanf("%d", &b->id);
	printf("Enter Name: ");             scanf("%s", b->name);
	printf("Enter Author (Fn Ln): ");   scanf("%s %s", b->author_name.fname,b->author_name.lname);
	printf("Enter Subject: ");          scanf("%s", b->subject);
	printf("Enter Price: ");  	        scanf("%lf", &b->price);
	printf("Enter ISBN: ");   	        scanf("%s", b->isbn);
}

void book_display(book_t *b)
{
	printf("\nID: %d", b->id);
	printf(" | Name: %s", b->name);
	printf("\t | Author: %s %s", b->author_name.fname,b->author_name.lname);
	printf("\t | Subject: %s", b->subject);
	printf("\t | Price: %0.2lf", b->price);
	printf("\t | ISBN: %s", b->isbn);
}

int get_next_book_id()
{
    FILE *fp;
    int max = 0;
    int size = sizeof(book_t);
    book_t b;
    // open the file
    fp = fopen(BOOK_DB,"rb");
    if (fp == NULL)
        return max + 1;    
	
	// change file pos to the last record
    fseek(fp, -size, SEEK_END);
	// read the record from the file
    if(fread(&b, size, 1, fp) > 0)
        // if read is successful, get max (its) id
        max = b.id;	

	// close the file
    fclose(fp);
	// return max + 1
    return max +1;
}


void book_find_by_name(char name[]) 
{
	FILE *fp;
	int found = 0;
	book_t b;
	// open the file for reading the data
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL) 
    {
		perror("\nFailed to open books file");
		return;
	}
	// read all books one by one
	while(fread(&b, sizeof(book_t), 1, fp) > 0) 
    {
		// if book name is matching partially, found 1
		if(strstr(b.name, name) != NULL) {
			found = 1;
			book_display(&b);
		}
	}
	// close file
	fclose(fp);
	if(!found)
		printf("No such book found.\n");
}

// bookcopy functions
void bookcopy_accept(bookcopy_t *c) 
{
	// printf("id: ");          // scanf("%d", &c->id);
	printf("Enter book id: ");	scanf("%d", &c->bookid);
	printf("Enter rack: ");   	scanf("%d", &c->rack);
	strcpy(c->status, STATUS_AVAIL);
}

void bookcopy_display(bookcopy_t *c) 
{
	printf("\nBookcopy id : %d |\t Book id : %d |\t Rack : %d |\t Status : %s", c->id, c->bookid, c->rack, c->status);
}

int get_next_bookcopy_id() 
{
	FILE *fp;
	int max = 0;
	int size = sizeof(bookcopy_t);
	bookcopy_t u;
	// open the file
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}

// issuerecord functions
void issuerecord_accept(issuerecord_t *r) 
{
	// printf("id: ");          // scanf("%d", &r->id);
	printf("Enter copy id: ");      scanf("%d", &r->copyid);
	printf("Enter member id: ");  	scanf("%d", &r->memberid);
	printf("Enter issue ");  		date_accept(&r->issue_date);
	//r->issue_date = date_current();
	r->return_duedate = date_add(r->issue_date, BOOK_RETURN_DAYS);
	memset(&r->return_date, 0, sizeof(date_t));
	r->fine_amount = 0.0;
}

void issuerecord_display(issuerecord_t *r) 
{
	printf("\nIssue record id: %d \t| Copy id: %d \t| Member id: %d \t| Fine: %.2lf", r->id, r->copyid, r->memberid, r->fine_amount);
	printf("\nIssue ");    		 date_print(&r->issue_date);
	printf("Return due ");   date_print(&r->return_duedate);
	printf("Return date ");  date_print(&r->return_date);
}

int get_next_issuerecord_id() 
{
	FILE *fp;
	int max = 0;
	int size = sizeof(issuerecord_t);
	issuerecord_t u;
	// open the file
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}

void profile_edit_by_id(int id)
{
	FILE *fp;
	int found = 0;
	user_t u;
	// open user file
	fp = fopen(USER_DB,"rb+");
	if (fp == NULL)
	{
		perror("\nFailed to open user file.");
		exit(1);
	}
	// read user one by one and check if user with given id is found
	while(fread(&u,sizeof(user_t),1,fp) > 0)
	{
		if (id == u.id)
		{
			found = 1;
			break;
		}
	}
	// if found
	if (found)
	{
		// input new user details from user
		long size = sizeof(user_t);
		char n_ph[15];
		int n_role;
		name_t n_name;
		user_t nu;
		nu.id = u.id;
		printf("Enter name : ");			scanf("%s %s", n_name.fname,n_name.lname);
		strcpy(nu.name.fname, n_name.fname);
		strcpy(nu.name.lname, n_name.lname);
		strcpy(nu.email, u.email);
		printf("Enter phone number : ");	scanf("%s", n_ph);
		strcpy(nu.phone, n_ph);
		strcpy(nu.password, u.password);
		do
		{
			printf("\nEnter user type --> 1.Owner \t 2.Librarian \t 3.Member : ");
			scanf("%d", &n_role);
		} while (n_role == 0 || n_role >= 4);

		if (n_role == 1)
			strcpy(nu.role, ROLE_OWNER);
		else if (n_role == 2)
			strcpy(nu.role, ROLE_LIBRARIAN);
		else 
			strcpy(nu.role, ROLE_MEMBER);
		// take file position one record behind
		fseek(fp, -size, SEEK_CUR);
		// overwrite user details into file
		fwrite(&nu, size, 1, fp);
		printf("\nUser Profile updated.");
	}
	else // if not found
		//show message to user that user not found
		printf("\nUser not found.");
	// close book file
	fclose(fp);
}

void change_password(int id)
{
	FILE *fp;
	int found = 0;
	char new_password[10], old_password[10];
	user_t u;
	int size = sizeof(user_t);
	printf("\nEnter current password : ");	scanf("%s",old_password);
	// open user file
	fp = fopen(USER_DB,"rb+");
	if (fp == NULL)
	{
		perror("\nFailed to open user file.");
		return;
	}
	// read user one by one and check if user with given id is found
	while(fread(&u,sizeof(user_t),1,fp) > 0)
	{
		if (u.id == id && strcmp(u.password, old_password) == 0)
		{
			found = 1;
			break;
		}
	}
	// if found
	if (found)
	{
		// input new password from user
		printf("\nEnter new password : ");	scanf("%s",new_password);
		// assign new user password to the user
		strcpy(u.password, new_password);
		// take file position one record behind
		fseek(fp, -size, SEEK_CUR);
		// overwrite user password details into file
		fwrite(&u, size, 1, fp);
		printf("\nUser password updated.");
	}
	else // if not found
		//show message to user that user not found
		printf("\nUser not found.");
	// close book file
	fclose(fp);
}

// payment functions
void payment_accept(payment_t *p)
{
	//printf("id: ");				scanf("%d", &p->id);
	printf("Enter member id: ");	scanf("%d", &p->memberid);
	//printf("type (fees/fine): ");	scanf("%s", p->type);
	strcpy(p->type, PAY_TYPE_FEES);
	printf("Enter amount: ");		scanf("%lf", &p->amount);
	p->tx_time = date_current();	
	//if(strcmp(p->type, PAY_TYPE_FEES) == 0)
	p->next_pay_duedate = date_add(p->tx_time, MEMBERSHIP_MONTH_DAYS);
	//else
	//	memset(&p->next_pay_duedate, 0, sizeof(date_t));

}

void payment_display(payment_t *p)
{
	printf("\nPayment: %d \t| Member id: %d \t| Type: %s \t| Amount: %.2lf\n", p->id, p->memberid, p->type, p->amount);
	printf("\nPayment "); 		date_print(&p->tx_time);
	printf("\nPayment due ");	date_print(&p->next_pay_duedate);
}

int get_next_payment_id() 
{
	FILE *fp;
	int max = 0;
	int size = sizeof(payment_t);
	payment_t pay;
	// open the file
	fp = fopen(PAYMENT_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&pay, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = pay.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}

//************************************************************************
void all_users_display()
{
	FILE *fp;
	user_t u;
	int count = 0;
	// open user file
	fp = fopen(USER_DB, "rb");
	if (fp == NULL)
	{
		perror("\nFailed to open user file.");
		return;
	}
	// read user one by one till eof
	while(fread(&u, sizeof(user_t), 1, fp) > 0)
	{
			user_display(&u);
			count++;
	}
	printf("\nNumber of users : %d",count);
	// close file
	fclose(fp);	
}

