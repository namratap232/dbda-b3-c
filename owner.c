#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void owner_area(user_t *u) 
{
    printf("\n***Owner Menu***");
	int choice;
	do {
		printf("\n\n0. Sign Out\n1. Appoint Librarian\n2. Edit Profile\n3. Change Password\n4. Fees/Fine Report\n5. Book Availability\n6. Book Categories/Subjects\n7. Display users\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) 
        {
			case 1: //Appoint Librarian
                appoint_librarian();
				break;
			case 2: //Edit Profile
                profile_edit_by_id(u->id);
				break;
			case 3: //Change Password
				change_password(u->id);
				break;
			case 4: //Fees/Fine Report
				break;
			case 5: //Book Availability
				bookcopy_checkavail_details();
				break;
			case 6: //Book Categories/Subjects
				break;
			case 7: //Display all users
				all_users_display();
				break;
		}
	}while (choice != 0);	
}

void appoint_librarian()
{
    // input librarian details
    user_t u;
    user_accept(&u);
    // change user role to librarian
    strcpy(u.role, ROLE_LIBRARIAN);
    // add librarian into the user's file
    user_add(&u);
}

